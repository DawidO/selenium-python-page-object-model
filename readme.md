# Automation Testing Of User Registration Form
Simple example of using the Selenium framework and Page Object model to automate a registration page.

## Prerequisites

- Python 3.10+
- Up-to-date Chrome and Firefox browsers
- Testmail.app account required

## Setup on Windows

- Open terminal
- Navigate to project directory
- Create and activate virtual environment by running:

```bash
venv-install.bat
```
- Install dependencies:

```bash
pip install -r requirements.txt
```

## Testmail.app 

Testmail.app account is required for the proper functioning of the tests. 
In the root directory create .env file. Add the following:

```bash
TESTMAIL_APIKEY = <YOUR APIKEY>
TESTMAIL_NAMESPACE = <YOUR NAMESPACE>
```
APIKEY and NAMESPACE can be found in testmail.app console. To have access to console you have to signed up.(link below) 
- https://testmail.app/signup

## Start tests

Some tests that are using testmail app to retrieve confirmation email are marked slow. They will be skipped by default. To run this tests use: 
```
python -m pytest --runslow
```