import os
from dotenv import load_dotenv

load_dotenv(override=True)

# Testmail.app API keys
APIKEY = os.getenv('TESTMAIL_APIKEY')
NAMESPACE = os.getenv('TESTMAIL_NAMESPACE')
EMAIL = f'{NAMESPACE}.*@inbox.testmail.app'


def check_testmail_credential():
    if None not in (APIKEY, NAMESPACE):
        return False
    else:
        return True


# URL and email
REGISTRATION_URL = 'https://www.sklepdemo.pl/rejestracja'
SENDER_EMAIL = 'test@sklepdemo.pl'
