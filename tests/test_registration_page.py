import pytest
from .base_test import TestBase
from pages.registration_page import RegistrationPage
from config import check_testmail_credential


@pytest.mark.usefixtures('unique_email_and_tag')
class TestRegistrationPage(TestBase):
    ERROR_MSG = {
        'email': 'Podaj prawidłowy email',
        'password': 'Co najmniej 5 znaków (bez spacji)',
        'rpassword': 'Hasła są różne',
        'terms_and_conditions': 'Akceptacja jest wymagana, aby przejść dalej',

    }
    success_msg = 'Konto zostało poprawnie aktywowane, zapraszamy do zakupów!'

    @pytest.fixture(autouse=True)
    def setup(self):
        self.register = RegistrationPage(self.driver)
        yield
        self.driver.delete_all_cookies()

    def test_register_with_empty_form(self):
        assert self.register.get_current_url() == self.register._url
        self.register._with(data={})
        assert self.ERROR_MSG['email'] == self.register.display_error_message('email')
        assert self.ERROR_MSG['password'] == self.register.display_error_message('password')
        assert self.ERROR_MSG['terms_and_conditions'] == self.register.display_error_message('terms_and_conditions')
        assert self.register.count_error_messages() == 3

    def test_register_with_invalid_email(self):
        self.register._with(data={
            'email': 'invalid_email',
            'password': '123qwe',
            'rpassword': '123qwe',
            'newsletter': True,
            'terms_and_conditions': True,
        })
        assert self.ERROR_MSG['email'] == self.register.display_error_message('email')
        assert self.register.count_error_messages() == 1

    def test_register_with_terms_and_conditions_not_selected(self):
        self.register._with(data={
            'email': 'iko.asdas@vp.ppl',
            'password': '123qwe',
            'rpassword': '123qwe',
            'newsletter': True,
            'terms_and_conditions': False,
        })
        assert self.ERROR_MSG['terms_and_conditions'] == self.register.display_error_message('terms_and_conditions')
        assert self.register.count_error_messages() == 1

    def test_register_without_password(self):
        self.register._with(data={
            'email': 'validmail@gggg.pl',
            'password': '',
            'rpassword': '1223qwe',
            'newsletter': True,
            'terms_and_conditions': True,
        })
        assert self.ERROR_MSG['password'] == self.register.display_error_message('password')
        assert self.ERROR_MSG['rpassword'] == self.register.display_error_message('rpassword')
        assert self.register.count_error_messages() == 2

    def test_register_with_password_less_then_minimum_length(self):
        self.register._with(data={
            'email': 'validmail@gggg.pl',
            'password': 'dom',
            'rpassword': 'dom',
            'newsletter': True,
            'terms_and_conditions': True,
        })
        assert self.ERROR_MSG['password'] == self.register.display_error_message('password')
        assert self.register.count_error_messages() == 1

    def test_register_with_different_confirmation_password(self):
        self.register._with(data={
            'email': 'validmail@gggg.pl',
            'password': '123qwe',
            'rpassword': 'qweqwe',
            'newsletter': True,
            'terms_and_conditions': True,
        })
        assert self.register.count_error_messages() == 1
        assert self.ERROR_MSG['rpassword'] == self.register.display_error_message('rpassword')

    @pytest.mark.skipif(check_testmail_credential(), reason="A config parameter (APIKEY, NAMESPACE) is missing.")
    @pytest.mark.slow
    def test_register_valid_required_data(self):
        self.register._with(data={
            'email': self.unique_email,
            'password': '123qwe',
            'rpassword': '123qwe',
            'newsletter': False,
            'terms_and_conditions': True,
        })
        assert self.register.count_error_messages() == 0
        assert self.register.confirmation_email_received(self.unique_email, self.test_tag) == self.success_msg

    @pytest.mark.skipif(check_testmail_credential(), reason="A config parameter (APIKEY, NAMESPACE) is missing.")
    @pytest.mark.slow
    def test_register_with_email_already_registered(self):
        self.register._with(data={
            'email': self.unique_email,
            'password': '123qwe',
            'rpassword': '123qwe',
            'newsletter': False,
            'terms_and_conditions': True,
        })
        assert self.register.count_error_messages() == 0
        assert self.register.fail_registration_message() == 'Podany email nie może zostać zarejestrowany'
