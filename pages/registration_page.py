from .base_page import BasePage
from config import REGISTRATION_URL, SENDER_EMAIL
from .locators import REGISTRATION_LOCATORS
from helpers import check_mailbox


class RegistrationPage(BasePage):
    _url = REGISTRATION_URL
    _form = REGISTRATION_LOCATORS['registration_form']
    _locators = REGISTRATION_LOCATORS
    _email_address = SENDER_EMAIL

    def __init__(self, driver):
        super().__init__(driver)
        self.go_to(self._url)

    def current_url(self):
        return self.get_current_url()

    def _with(self, data=False):
        """
        Fills in form with given data. By default, empty form will be submitted.

        :param data: (dict) This argument, if given, should be a dictionary mapping field names to test value.
        """
        if data:
            fields = data.keys()
            for field in fields:
                locator = self._form[field]
                if locator['type'] == 'input_text':
                    self.fill(locator, data[field])
                elif locator['type'] == 'checkbox':
                    self.click_checkbox(locator, data[field])
        self.submit_btn()

    def submit_btn(self):
        # Submit registration form
        self.click_btn(self._locators['submit_btn'], 3)

    def display_error_message(self, field):
        """
        Return error message related to form filed if present.
        :param field: (str) input field name
        :return:
            error_message (str)
        """
        return self.get_message(self._form[field]['error_msg'], wait=3)

    def fail_registration_message(self):
        return self.get_message(self._locators['registration_fail_msg'])

    def count_error_messages(self):
        # Return numbers of error messages
        errors = self.find_elements(self._locators['error_msgs'])
        return len(errors)

    def confirmation_email_received(self, email, tag):
        confirmation_link = check_mailbox(
            sender=self._email_address,
            receiver=email,
            subject='Rejestracja użytkownika',
            tag=tag)
        if confirmation_link:
            self.driver.get(confirmation_link)
            return self.get_message(self._locators['registration_success_msg'])
        else:
            return False


